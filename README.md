# REAL Mental Health Take Home Assessment
By Christopher Ronning

### Startup Instructions

To set up the server, make sure you're in the `server/` directory.

There is some test data included in `server/src/data/` as a starting point to seed your database. Feel free to add or change data.

The database is setup via Docker. There is already a `docker-compose.yml` file in the server directory, so if you have Docker installed, just run:
```bash
docker-compose up -d
```

To create the DB Tables and Seed the Database, we need to install all our server packages. First run:
```bash
yarn
```

Then to create DB Tables, run:
```bash
yarn db:run
```

Then to seed the DB, run:
```bash
yarn db:seed
```

To start the server, run:
```bash
yarn start
```

To run the frontend, make sure you're in the `course-client/` directory, and run:
```bash
yarn start
```

### Comments

I've structured this application as RESTfully as possible given the time I had to complete it. All routes related to fetching course data and registering are declared in the `courses` controller. As course sections and course sessions will never be considered without reference to their owner courses, I've considered them to be aspects of the `course` resource.

In order to associate `user`s with `course_section`s, I needed to do a few things. First, I needed to create a many-to-many relationship within the entity definition. Once I did this, I needed to generate a migration and create a new join table. 

I was confused, because the database was initially generated with a `user_course_sections_course_section`... but with no way to update it, because there was no relationship defined on the entity definitions. When I created those relationships and saw that I ran into errors trying to access them, I ran `yarn db:migrate` out of curiosity. This wound up generating a new join table, `course_section_users_user` which I used in place of the pre-existing one. I would be interested to hear if I was missing some feature of TypeORM!

On that note, this is the first time I've used TypeORM, and I'm super impressed! It reminds me a bit of .NET's Entity Framework -- which is a very good thing, in my opinion :)

Final comment: While the instructions specified that the user could not register for a course that took place in the past, the seed data only included course sections in 2020. I took the liberty of updating this for my own testing.

### TODO List

I consider this application to be solid but suboptimal, given the amount of time I had to complete it. In a perfect world, I would love to do the following:

1. Implement front-end routing via React Router, to make it easier to manage navigation, and allow users to bookmark their own course pages.
2. Implement a separate page for each course session.
3. Fix a strange bug, where Express throws the following error: `Error [ERR_HTTP_HEADERS_SENT]: Cannot set headers after they are sent to the client`. 
4. Clean up my services code, and re-evaluate the way I've structured it. Specifically, I would like to learn the TypeORM API better, and avoid doing some of the intensive object manipulation in e.g. `CourseService.all`.
5. Implement a page for each course, allowing users to both register and view individual sessions. Along with this, I would create more API endpoints to fetch individual courses with their sections, as well as individual sessions.

import '../styles/CourseCatalog.css';

import React, { useContext, useState } from 'react';
import PropTypes from 'prop-types';
import { UserContext } from './hoc/withUser';
import axiosClient from '../axiosClient';

const SECTION_COMPLETED = 'SECTION_COMPLETED';
const TOO_MANY_USERS = 'TOO_MANY_USERS';

const CourseCatalog = ({ courseList, selectCourse, updateCourses }) => {
  const { user } = useContext(UserContext);
  const [registrationError, setRegError] = useState(null);

  const registerForSection = async (courseId, sectionId) => {
    try {
      const registerResponse = await axiosClient.post(`courses/${courseId}/sections/${sectionId}`, {
        userId: user.id
      });

      updateCourses(courseId, sectionId);
    } catch (e) {
      console.log(e);
      if (e.response.data.code === SECTION_COMPLETED) {
        setRegError({
          sectionId,
          message: e.response.data.message
        });
      }
      if (e.response.data.code === TOO_MANY_USERS) {
        setRegError({
          sectionId,
          message: e.response.data.message
        })
      }
    }

  }
  if (!courseList) {
    return null;
  }

  return (
    <div id="course-catalog">
      <h2>Courses</h2>
      <ul>
      {courseList.map(course => (
        <li key={course.id}>
          <div className="course-desc">
            <h2>{course.name}</h2>
            <p>{course.description}</p>
          </div>
          <div className="course-sections">
            <h3>Sections</h3>
            <ul>
              {course.courseSections.map(section => {
                const date = new Date(section.startDate);
                return (
                  <li key={section.id}>
                    <h4 className="section-nick">{section.nickname}</h4>
                    <p>Start Date: {date.getMonth() + 1}/{date.getDate()}/{date.getFullYear()} </p>
                    {section.registered ? 
                      <div>Registered! <a className="view-link" onClick={() => selectCourse(course)}>View Course Sessions</a></div> :
                      <a onClick={() => registerForSection(course.id, section.id)}>Register for this section</a>
                    }
                    { registrationError && registrationError.sectionId === section.id && 
                      <p className="error">{registrationError.message}</p>}
                  </li>
                );
              })}
            </ul>
          </div>
        </li>
      ))}
      </ul>
    </div>
  )
};

CourseCatalog.propTypes = {
  courseList: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    courseSections: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number.isRequired,
      nickname: PropTypes.string.isRequired,
      startDate: PropTypes.string.isRequired,
      registered: PropTypes.bool,
    })).isRequired
  })),
  updatePage: PropTypes.func.isRequired,
  updateCourses: PropTypes.func.isRequired,
};

export default CourseCatalog;

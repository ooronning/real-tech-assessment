import '../styles/CourseSessions.css';

import React, { useEffect, useState } from 'react';
import axiosClient from '../axiosClient';
import { PageView } from './Courses';

const CourseSessions = ({course, userId, setPage }) => {
  
  const [sessions, setSessions] = useState([]);
  useEffect(() => {
    const updateSessions = async () => {
      const res = await axiosClient.get(`/courses/${course.id}/sessions`, { params: { userId }});
      console.log(res.data);
      setSessions(res.data);
    }

    updateSessions();
  }, []);

  if (sessions.length === 0) {
    return <div>Loading</div>;
  }

  return (
    <div id="course-sessions">
      <span><a id="back-link" onClick={() => setPage(PageView.Catalog)}>Back to Courses</a></span>
      <h2>{course.name}</h2>
      {sessions.map(session => (
        <div key={session.sessionNumber}>
          <h3>{session.sessionNumber}. {session.name}</h3>
          <p>{session.content}</p>
        </div>
      ))}
    </div>
  );
}


export default CourseSessions;

import React, { useState, useEffect } from 'react';
import axiosClient from '../axiosClient';
import CourseCatalog from './CourseCatalog';
import CourseSessions from './CourseSessions';

export const PageView = {
  Catalog: 'Catalog',
  CourseSessions: 'CourseSessions'
};

export const Courses = ({ user }) => {
  const [courseList, setCourseList] = useState(null);
  const [selectedCourse, selectCourse] = useState(null);
  const [page, setPage] = useState(PageView.Catalog);
  const [error, setError] = useState(null);

  const updateAfterRegistration = (courseId, sectionId) => {
    const courses = [...courseList];
    for (const course of courses) {
      if (courseId === course.id) {
        const section = course.courseSections.find(section => sectionId === section.id);
        section['registered'] = true;
      }
    }

    setCourseList(courses);
  }

  useEffect(() => {
    const loadCourses = async () => {
      try {
        const res = await axiosClient.get('/courses', { params: { userId: user.id }});
        setCourseList(res.data);
      } catch (e) {
        setError('An error has occurred, please refresh.');
      }
    };

    loadCourses();
  }, [user]);

  useEffect(() => {
    if (selectedCourse) {
      setPage(PageView.CourseSessions);
    }
  }, [selectedCourse]);

  return (
    <>
      <h1>Welcome: {user.username} </h1>
      {error ? <p>{error}</p> : null}
      {page === PageView.Catalog && 
        <CourseCatalog 
          courseList={courseList} 
          selectCourse={selectCourse}
          updateCourses={updateAfterRegistration}  
        />}
      {page === PageView.CourseSessions && 
        <CourseSessions 
          course={selectedCourse} 
          userId={user.id} 
          setPage={setPage}  
        />
      }
    </>
  );
};

export const CourseSectionSeed = [
	{
		courseId: 1,
		nickname: 'Section 1',
		startDate: '2021-1-18',
	},
	{
		courseId: 1,
		nickname: 'Section 2',
		startDate: '2021-11-13',
	},
	{
		courseId: 1,
		nickname: 'Section 3',
		startDate: '2021-11-27',
	},
	{
		courseId: 2,
		nickname: 'Section 1',
		startDate: '2021-10-15',
	},
	{
		courseId: 2,
		nickname: 'Section 2',
		startDate: '2021-10-29',
	},
	{
		courseId: 2,
		nickname: 'Section 3',
		startDate: '2021-11-12',
	},
];

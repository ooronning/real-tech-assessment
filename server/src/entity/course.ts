import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { CourseSection } from './course-section';

@Entity()
export class Course {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @OneToMany(() => CourseSection, course_section => course_section.course)
  courseSections: CourseSection[];
}

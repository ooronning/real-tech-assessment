import {Column, Entity, PrimaryGeneratedColumn, ManyToMany, JoinTable} from "typeorm";
import { CourseSection } from './course-section';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column()
  email: string;

  @JoinTable()
  @ManyToMany(type => CourseSection)
  courseSections: CourseSection[];
}

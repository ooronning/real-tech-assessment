import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateJoinTableUsersCourseSections1611119926138 implements MigrationInterface {
    name = 'CreateJoinTableUsersCourseSections1611119926138'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "course_section_users_user" ("courseSectionId" integer NOT NULL, "userId" integer NOT NULL, CONSTRAINT "PK_8fd3542bc090c3ea66b97605123" PRIMARY KEY ("courseSectionId", "userId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_04b56a3ae302535b3f37dbd846" ON "course_section_users_user" ("courseSectionId") `);
        await queryRunner.query(`CREATE INDEX "IDX_09efa7aa4dacba6839e2568fa1" ON "course_section_users_user" ("userId") `);
        await queryRunner.query(`ALTER TABLE "course_section_users_user" ADD CONSTRAINT "FK_04b56a3ae302535b3f37dbd846b" FOREIGN KEY ("courseSectionId") REFERENCES "course_section"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "course_section_users_user" ADD CONSTRAINT "FK_09efa7aa4dacba6839e2568fa1d" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "course_section_users_user" DROP CONSTRAINT "FK_09efa7aa4dacba6839e2568fa1d"`);
        await queryRunner.query(`ALTER TABLE "course_section_users_user" DROP CONSTRAINT "FK_04b56a3ae302535b3f37dbd846b"`);
        await queryRunner.query(`DROP INDEX "IDX_09efa7aa4dacba6839e2568fa1"`);
        await queryRunner.query(`DROP INDEX "IDX_04b56a3ae302535b3f37dbd846"`);
        await queryRunner.query(`DROP TABLE "course_section_users_user"`);
    }

}

import { Router } from 'express';
import { CourseService } from '../services/course.service';
import { CourseSectionService, TOO_MANY_USERS } from '../services/course-section.service';
import { CourseSessionService } from '../services/course-session.service';
import { UserService } from '../services/user.service';

export const CourseRoutes = () => {
  const router = Router();
  const courseService = new CourseService();
  const courseSectionService = new CourseSectionService();
  const courseSessionService = new CourseSessionService();
  const userService = new UserService();

  router.get('/', async (req, res, next) => {
    // Fake userId in query param in lieu of sessions
    const userId = Number(req.query.userId);
    try {
      const courses = await courseService.all(userId);
      res.status(200).json(courses);
      next();
    } catch (err) {
      res.status(400).json({ message: 'BAD_REQUEST' });
      next();
    }
  });

  router.post('/:courseId/sections/:sectionId', async (req, res, next) => {
    const courseId = Number(req.params.courseId);
    const sectionId = Number(req.params.sectionId);
    // Since we don't have user sessions, I'm faking it by including the user
    // id from the client in the body. Normally, we would do this using 
    // sessions or something similar.
    const userId = Number(req.body.userId);

    try {
      const course = await courseService.one(courseId);
      if (!course) {
        res.status(404).json({ message: 'BAD_REQUEST: Invalid course ID provided'});
        next();
      }

      const section = await courseSectionService.one(sectionId);
      if (!section) {
        res.status(404).json({ message: 'BAD_REQUEST: Invalid course section ID'});
        next();
      }

      if (new Date(section.startDate) < new Date(Date.now())) {
        res.status(400).json({ 
          code: 'SECTION_COMPLETED',
          message: 'The requested course section has already completed.'
        });
        next();
      }

      const user = await userService.one(userId);
      if (!user) {
        res.status(400).json({ message: 'BAD_REQUEST: Invalid user ID'});
        next();
      }

      // destructure response to avoid returning a list of registered users
      const {
        id,
        nickname,
        startDate,
        courseId: cid,
      } = await courseSectionService.register(sectionId, userId);
      const data = { id, nickname, startDate, courseId: cid };
      res.status(201).json({message: 'SUCCESS', data });
      next();
    } catch (e) {
      if (e.message === TOO_MANY_USERS) {
        res.status(400).json({ code: TOO_MANY_USERS ,message: 'This class is unavailable '});
        next();
      }
      res.status(400).json({ message: 'BAD_REQUEST' });
      next();
    }
  });

  router.get('/:courseId/sessions', async (req, res, next) => {
    // Fake userId in query param in lieu of sessions
    const userId = Number(req.query.userId);
    const courseId = Number(req.params.courseId);
    
    try {
      const course = await courseService.one(courseId);
      if (!course) {
        res.status(404).json({ message: 'BAD_REQUEST: Invalid course ID provided'});
        next();
      }

      const user = await userService.one(userId);
      if (!user) {
        res.status(400).json({ message: 'BAD_REQUEST: Invalid user ID'});
        next();
      }
      const isUserRegistered = await courseService.isUserRegistered(courseId, userId);
      if (!isUserRegistered) {
        res.status(400).json({ message: `User ${userId} is not registered for this course`});
        next();
      }

      const sessions = await courseSessionService.getForCourse(courseId);
      res.status(200).json(sessions);
      next();
    } catch (e) {
      res.status(400).json({ message: 'BAD_REQUEST' });
      next();
    }
  });

  return router;
}

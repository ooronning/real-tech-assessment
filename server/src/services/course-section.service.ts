import { CourseSection } from '../entity/course-section';
import { getRepository } from 'typeorm';
import { CourseService } from './course.service';
import { UserService } from './user.service';

export const TOO_MANY_USERS = 'TOO_MANY_USERS';

export class CourseSectionService {
  private courseSectionRespository = getRepository(CourseSection);
  private userService = new UserService();

  async one(id: number) {
    return await this.courseSectionRespository.findOne(id);
  }

  async register(sectionId: number, userId: number) {
    const section = await this.courseSectionRespository.findOne({
      where: { id: sectionId },
      relations: ['users']
    });
    const user = await this.userService.one(userId);
    if (!!section.users && section.users.length >= 5) {
      throw new Error(TOO_MANY_USERS);
    }
    if (!!section.users && section.users.length > 0) {
      section.users = [...section.users, user];
    } else {
      section.users = [user];
    }
    
    return await this.courseSectionRespository.save(section);
  }
}

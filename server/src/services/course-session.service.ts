import { CourseSession } from '../entity/course-session';
import { getRepository } from 'typeorm';

export class CourseSessionService {
  private sessionRepository = getRepository(CourseSession);

  async getForCourse(courseId: number) {
    return await this.sessionRepository.find({
      where: { courseId }
    });

    
  }
}

import { Course } from '../entity/course';
import { getRepository } from 'typeorm';
import { User } from '../entity/user';

export class CourseService {
  private courseRepository = getRepository(Course);
  private userRepository = getRepository(User);

  async all(userId: number) {
    const courses = await this.courseRepository.find({
      relations: ['courseSections', 'courseSections.users']
    });

    // find course sections in which userId is in courseSections.users
    // mark as registered
    for (const course of courses) {
      const registeredSections = course.courseSections.filter(section => {
        return !!section.users.find(user => user.id === userId);
      });

      for (const section of course.courseSections) {
        const match = !!registeredSections.find(s => s.id === section.id);
        if (match) {
          section['registered'] = true;
        } else {
          section['registered'] = false;
        }
        
        // in production, I would not do this
        delete section.users;
      }
    }

    return courses; 
  }

  async one(id: number) {
    return await this.courseRepository.findOne(id);
  }

  async isUserRegistered(courseId: number, userId: number) {
    const courses = await this.courseRepository.find({
      relations: ['courseSections', 'courseSections.users'],
      where: { id: courseId },
    });

    const sections = courses.map(course => course.courseSections).flat();
    const isRegistered = !!sections.find(section => {
      return !!section.users.find(user => user.id === userId)
    });

    return isRegistered;
  }
}
